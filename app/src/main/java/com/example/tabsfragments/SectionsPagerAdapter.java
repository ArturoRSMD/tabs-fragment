package com.example.tabsfragments;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.tabsfragments.R;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {


    String defaultTitlePage = "Default";
    String titlePage1 = "Artist";
    String titlePage2 = "Song";

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new ArtistFragment() ;
            case 1:
                return new SongFragment();
                default:
                    return null;
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return titlePage1;
            case 1:
                return titlePage2;
                default:
                    return defaultTitlePage;
        }
    }

    // Show 2 total pages.

    @Override
    public int getCount() {
        return 2;
    }
}